package com.example.blogup.tarneemapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RigesterPage extends AppCompatActivity implements View.OnClickListener {


    EditText txtFullName, txtEmail, txtPhone, txtPass, txtConfirmPass;

    EditText txtD, txtM, txtY;

    RadioGroup RG;

    Button btnRegisters;

    FirebaseAuth auth;

    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rigester_page);


        init();
    }


    void init() {
        auth = FirebaseAuth.getInstance();

        reference = FirebaseDatabase.getInstance().getReference();

        txtFullName = findViewById(R.id.txtFullName);

        txtEmail = findViewById(R.id.txtEmail);

        txtPhone = findViewById(R.id.txtPhone);

        txtPass = findViewById(R.id.txtPass);

        txtConfirmPass = findViewById(R.id.txtConfirmPass);

        RG = findViewById(R.id.RG);

        txtD = findViewById(R.id.txtD);
        txtM = findViewById(R.id.txtM);
        txtY = findViewById(R.id.txtY);


        btnRegisters = findViewById(R.id.btnRegisters);
        btnRegisters.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnRegisters:
                ButtonRegisters();
                break;

        }

    }

    private void ButtonRegisters() {

        final String FullName = txtFullName.getText().toString();

        final String Email = txtEmail.getText().toString().trim();

        final String PhoneNumber = txtPhone.getText().toString();

        final String password = txtPass.getText().toString();

        final String ConfirmPass = txtConfirmPass.getText().toString();


        final Map<String, String> BirthDay = new HashMap<>();
        BirthDay.put("Day", txtD.getText().toString());
        BirthDay.put("Month", txtM.getText().toString());
        BirthDay.put("Year", txtY.getText().toString());

        RadioButton Selected;

        int idRadioButton = RG.getCheckedRadioButtonId();


        Selected = findViewById(idRadioButton);

        final String Gender = Selected.getText().toString();


        if (Email.equals("") || password.equals(""))
            return;


        if (password.equals(ConfirmPass)) {
            auth.createUserWithEmailAndPassword(Email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    if (task.isSuccessful()) {
                        Map<String, Object> map = new HashMap<>();

                        map.put("FullName", FullName);
                        map.put("Email", Email);
                        map.put("PhoneNumber", PhoneNumber);
                        map.put("BirthDay", BirthDay);
                        map.put("Gender", Gender);


                        reference.child("Users").child(auth.getCurrentUser().getUid()).setValue(map);

                        Intent intent = new Intent(RigesterPage.this, HomePage.class);
                        startActivity(intent);
                        finish();
                    }

                }
            });
        } else
            Toast.makeText(getApplicationContext(), "Password does not match", Toast.LENGTH_SHORT).show();


    }
}
