package com.example.blogup.tarneemapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LogInPage extends AppCompatActivity implements View.OnClickListener {

    Button btnReg  , btnLogin;

    EditText txtEmail , txtPass;


    FirebaseAuth auth ;

    ProgressBar progressBar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_page);

        init() ;
    }


    void init()
    {
        progressBar = findViewById(R.id.progressBar);
        auth = FirebaseAuth.getInstance() ;

        btnReg = findViewById(R.id.btnReg);

        btnReg.setOnClickListener(this);

        txtEmail = findViewById(R.id.txtEmail);

        txtPass = findViewById(R.id.txtPass);

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {

            case  R.id.btnReg : ButtonReg(); break;

            case R.id.btnLogin : ButtonLogin() ; break;


        }
    }

    private void ButtonLogin() {

        String Email = txtEmail.getText().toString();

        String password = txtPass.getText().toString() ;

        progressBar.setVisibility(View.VISIBLE);

        if(Email.equals("") && password.equals(""))
        {
            progressBar.setVisibility(View.GONE);
            return;
        }


        auth.signInWithEmailAndPassword(Email , password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful())
                {
                  Intent intent = new Intent(LogInPage.this , HomePage.class) ;
                  startActivity(intent);
                  finish();
                }
                else
                    Toast.makeText(getApplicationContext() , "false" , Toast.LENGTH_SHORT ).show();

                progressBar.setVisibility(View.GONE);
            }
        });


    }

    private void ButtonReg() {


        Intent intent = new Intent(LogInPage.this , RigesterPage.class);

        startActivity(intent);


    }
}
