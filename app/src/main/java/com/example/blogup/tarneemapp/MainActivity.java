package com.example.blogup.tarneemapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {


    FirebaseAuth auth ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        auth = FirebaseAuth.getInstance();

        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(2*1000);

                   if(auth.getCurrentUser()!= null)
                   {
                       Intent i=new Intent(getBaseContext(),HomePage.class);
                       startActivity(i);

                       //Remove activity
                       finish();

                   }
                   else
                   {
                       Intent i=new Intent(getBaseContext(),LogInPage.class);
                       startActivity(i);

                       //Remove activity
                       finish();
                   }

                } catch (Exception e) {
                }
            }
        };
        // start thread
        background.start();

    }
}
